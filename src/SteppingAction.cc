//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// -------------------------------------------------------------------
// -------------------------------------------------------------------

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

#include "Analysis.hh"

#include "SteppingAction.hh"
#include "RunAction.hh"
#include "DetectorConstruction.hh"
#include "PrimaryGeneratorAction.hh"

#include "G4SystemOfUnits.hh"
#include "G4SteppingManager.hh"
#include "G4VTouchable.hh"
#include "G4VPhysicalVolume.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

SteppingAction::SteppingAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

SteppingAction::~SteppingAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void SteppingAction::UserSteppingAction(const G4Step* step)
{
 G4double pid=0.;
 G4double proc=0.;
 G4double t0,x0,y0,z0,E0,t1,x1,y1,z1,E1,px,py,pz;

 pid = step->GetTrack()->GetDynamicParticle()->GetPDGcode();

 if (step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName()=="msc")			proc =10;
 if (step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName()=="e-_G4MicroElecElastic")	proc =11;
 if (step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName()=="e-_G4MicroElecInelastic")	proc =12;
 if (step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName()=="eCapture")			proc =13;

 if (step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName()=="p_G4MicroElecInelastic")	proc =14;

 if (step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName()=="ion_G4MicroElecInelastic")	proc =15;

 if (step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName()=="hIoni")			proc =16;
 if (step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName()=="eIoni")			proc =17;

 if (step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName()!="Transportation")
 {
   t0=step->GetPreStepPoint()->GetGlobalTime()/nanosecond;
   x0=step->GetPreStepPoint()->GetPosition().x()/nanometer;
   y0=step->GetPreStepPoint()->GetPosition().y()/nanometer;
   z0=step->GetPreStepPoint()->GetPosition().z()/nanometer;
   E0=step->GetPreStepPoint()->GetKineticEnergy()/electronvolt;
   t1=step->GetPostStepPoint()->GetGlobalTime()/nanosecond;
   x1=step->GetPostStepPoint()->GetPosition().x()/nanometer;
   y1=step->GetPostStepPoint()->GetPosition().y()/nanometer;
   z1=step->GetPostStepPoint()->GetPosition().z()/nanometer;
   E1=step->GetPostStepPoint()->GetKineticEnergy()/electronvolt;
   px=step->GetPostStepPoint()->GetMomentumDirection().x();
   py=step->GetPostStepPoint()->GetMomentumDirection().y();
   pz=step->GetPostStepPoint()->GetMomentumDirection().z();

   // get analysis manager
   G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

   // fill ntuple
   int i = 0;
   analysisManager->FillNtupleDColumn(i++, proc);
   analysisManager->FillNtupleDColumn(i++, pid);
   analysisManager->FillNtupleDColumn(i++, t0);
   analysisManager->FillNtupleDColumn(i++, x0);
   analysisManager->FillNtupleDColumn(i++, y0);
   analysisManager->FillNtupleDColumn(i++, z0);
   analysisManager->FillNtupleDColumn(i++, E0);
   analysisManager->FillNtupleDColumn(i++, t1);
   analysisManager->FillNtupleDColumn(i++, x1);
   analysisManager->FillNtupleDColumn(i++, y1);
   analysisManager->FillNtupleDColumn(i++, z1);
   analysisManager->FillNtupleDColumn(i++, E1);
   analysisManager->FillNtupleDColumn(i++, px);
   analysisManager->FillNtupleDColumn(i++, py);
   analysisManager->FillNtupleDColumn(i++, pz);
   analysisManager->FillNtupleDColumn(i++, step->GetTotalEnergyDeposit()/eV);
   analysisManager->FillNtupleDColumn(i++, std::sqrt((x1-x0)*(x1-x0)+(y1-y0)*(y1-y0)+(z1-z0)*(z1-z0)));
   analysisManager->FillNtupleDColumn(i++, (E0 - E1));
   analysisManager->AddNtupleRow();
 }
}
