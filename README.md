# BL3 Microelectronics Simulations

This repository contains the advanced/microelectronics example
and physics list from geant4. It has been modified to study the
energy deposition and backscattering of 30 keV protons on silicon
detectors to allow for comparisons with SRIM.

## Downloading

Download the code by cloning this repository:
```
git clone https://gitlab.com/BL3/microelectronics.git
```
This will create a directory `microelectronics` with the code.

## Compilation

To compile in a directory `build`, use the following commands:
```
cd microelectronics
mkdir build
cmake -B build .
make -C build
```

## Running

To run the simulation with the example `microelectronics.mac` macro, use
the following commands
```
build/microelectronics microelectronics.mac
```
This will generate a few (depending on the number of cores) root
files named `microelectronics_t0.root` etc.

## Analyzing

To analyze, you may be interested in the plot.C macro:
```
root plot.C
```
This will `hadd` the different root files together and plot a few
useful (or not so useful) histograms.
